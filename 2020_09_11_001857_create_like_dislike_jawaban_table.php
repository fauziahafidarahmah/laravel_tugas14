<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikeJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_jawaban', function (Blueprint $table) {
            $table->bigIncrements('jawaban_id');
            $table->foreign('jawaban_id')->reference('id')->on('jawaban');
            $table->bigIncrements('profil_id');
            $table->foreign('profil_id')->reference('id')->on('user');
            $table->bigIncrements('poin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_jawaban');{
            $table->dropForeign(['jawaban_id']);
            $table->dropColumn(['jawaban_id']);
            $table->dropColumn(['profil_id']);
            $table->dropForeign(['profil_id']);
        }
    }
}
