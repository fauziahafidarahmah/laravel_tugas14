<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_pertanyaan', function (Blueprint $table) {
            $table->unassignedBigInteger('pertanyaan_id');
            $table->foreign('pertanyaan_id')->reference('id')->on('pertanyaan_id');
            $table->unassignedBigInteger('profil_id');
            $table->foreign('profile_id')->reference('id')->on('user');
            $table->unassignedBigInteger('poin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_pertanyaan');{
            $table->dropForeign(['pertanyaan_id']);
            $table->dropColumn(['pertanyaan_id']);
            $table->dropColumn(['profile_id']);
            $table->dropForeign(['profile_id']);
        }
    }
}
